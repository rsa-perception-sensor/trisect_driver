#pragma once

#include <sensor_msgs/CameraInfo.h>

namespace trisect_camera_driver {

sensor_msgs::CameraInfo scaleCameraInfo(const sensor_msgs::CameraInfo &cam,
                                        double downsample = 1.0);

sensor_msgs::CameraInfoPtr scaleCameraInfo(
    const sensor_msgs::CameraInfoConstPtr &cam, double downsample = 1.0);

}  // namespace trisect_camera_driver
