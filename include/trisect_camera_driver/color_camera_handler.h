#pragma once

#include <diagnostic_updater/diagnostic_updater.h>
#include <diagnostic_updater/publisher.h>
#include <ros/ros.h>

#include <string>

#include "libtrisect/v4l_camera.h"
#include "trisect_camera_driver/trisect_camera_publisher.h"

namespace trisect_camera_driver {

class ColorCameraHandler {
 public:
  ColorCameraHandler(ros::NodeHandle &nh, ros::NodeHandle &pnh,
                     const std::string &name,
                     const std::shared_ptr<libtrisect::GstCamera> &camera,
                     diagnostic_updater::Updater &updater,
                     bool accelerated_image_proc = true);

  std::shared_ptr<libtrisect::GstCamera> camera() { return camera_; }

  int count() {
    if (image_pub_) {
      return image_pub_->count();
    } else {
      return 0;
    }
  }

  void start() {
    if (camera()) camera()->start();
  }

  void stop() {
    if (camera()) {
      camera()->setDone();
      camera()->join();
    }
  }

 private:
  std::string name_;

  std::shared_ptr<libtrisect::GstCamera> camera_;

  std::shared_ptr<TrisectCameraPublisher> image_pub_;
  std::shared_ptr<DownsamplePublisher> downsampled_pub_;

  std::shared_ptr<camera_info_manager::CameraInfoManager> info_manager_;

  double min_freq_, max_freq_;
  std::shared_ptr<diagnostic_updater::HeaderlessTopicDiagnostic> pub_freq_;

  image_transport::ImageTransport image_transport_;

  ros::Publisher metadata_pub_;
};

}  // namespace trisect_camera_driver
