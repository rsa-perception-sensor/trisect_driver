#pragma once

#include <vpi/Array.h>
#include <vpi/Stream.h>

#include "libtrisect/v4l_camera.h"
#include "trisect_camera_driver/bounded_value.h"

namespace trisect_camera_driver {

class ExposureCalculator {
  /// CPU-only exposure calculation
  ///

 public:
  typedef BoundedValue<libtrisect::Camera::ExposureType> ExposureValue;
  typedef BoundedValue<libtrisect::Camera::GainType> GainValue;

  const int64_t MAX_DELTA_EXPOSURE_NS = 25000000;

  ExposureCalculator(const libtrisect::V4LCamera::SharedPtr &camera,
                     ExposureValue::Type initialExposure = 0,
                     GainValue::Type initialGain = 0);

  virtual libtrisect::Camera::ExposureType calculate(
      const libtrisect::Camera::Buffer_t &image);

  ExposureValue &exposure() { return _calcExposure; }
  GainValue &gain() { return _calcGain; }

  void setExposureTarget(float target);
  void setExposureKp(float kp);

 protected:
  ExposureValue _calcExposure;
  GainValue _calcGain;

  float _exposureTarget;
  float exposure_kp_;
};

class VPIExposureCalculator : public ExposureCalculator {
  //
  // Exposure calculator which uses VPI acceleration
  //

 public:
  VPIExposureCalculator(const libtrisect::V4LCamera::SharedPtr &camera,
                        ExposureValue::Type initialExposure = 0,
                        GainValue::Type initialGain = 0);

  ~VPIExposureCalculator();

  libtrisect::Camera::ExposureType calculate(
      const libtrisect::Camera::Buffer_t &image) override;

 protected:
  VPIStream stream_;
  VPIArray output_;
};

}  // namespace trisect_camera_driver
