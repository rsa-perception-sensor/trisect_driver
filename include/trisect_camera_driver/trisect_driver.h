#pragma once

#include <diagnostic_updater/diagnostic_updater.h>
#include <diagnostic_updater/publisher.h>
#include <dynamic_reconfigure/server.h>
#include <ros/ros.h>

#include <boost/thread/recursive_mutex.hpp>

#include "code_timing/code_timing.h"
#include "libtrisect/gst_camera.h"
#include "libtrisect/trigger/trigger_service.h"
#include "libtrisect/utils/set_thread_name.h"
#include "libtrisect/v4l_camera.h"
#include "trisect_camera_driver/color_camera_handler.h"
#include "trisect_camera_driver/expected.hpp"
#include "trisect_camera_driver/exposure_calculator.h"
#include "trisect_camera_driver/mono_camera_handler.h"
#include "trisect_camera_driver/trisect_camera_publisher.h"

// Auto-generated from .cfg file
#include "trisect_camera_driver/TrisectCameraConfig.h"

namespace trisect_camera_driver {

class TrisectDriver {
 public:
  typedef std::shared_ptr<TrisectDriver> shared_ptr;
  typedef tl::expected<TrisectDriver::shared_ptr,
                       libtrisect::Camera::CameraFactoryError>
      FactoryExpected;

  // Initialization of the cameras could fail, so put that in a factory
  static FactoryExpected CreateTrisectDriver(ros::NodeHandle &nh,
                                             ros::NodeHandle &pnh);

  ~TrisectDriver();

  void start();
  void stop();

 protected:
  // Protected constructor;  call the factory function CreateTrisectDriver()
  // to instantiate an instance of this class.
  TrisectDriver(
      ros::NodeHandle &nh, ros::NodeHandle &pnh,
      const std::shared_ptr<libtrisect::V4LCamera> &leftCam,
      const std::shared_ptr<libtrisect::V4LCamera> &rightCam,
      const std::shared_ptr<libtrisect::GstCamera> &colorCam,
      const std::shared_ptr<libtrisect::TriggerService> &triggerService,
      bool accelerated_image_proc);

  void configureCamera(
      ros::NodeHandle &pnh, const std::string &name,
      const std::shared_ptr<libtrisect::Camera> &cam,
      const std::shared_ptr<camera_info_manager::CameraInfoManager> &info_mgr,
      std::shared_ptr<TrisectCameraPublisher> &pub);

  diagnostic_updater::Updater _updater;

  MonoCameraHandler left_, right_;
  ColorCameraHandler color_;

  // Make this a shared_ptr for safety when passing between lambdas
  std::shared_ptr<VPIExposureCalculator> exposure_calculator_;

  std::shared_ptr<libtrisect::TriggerService> trigger_client_;

  typedef dynamic_reconfigure::Server<
      trisect_camera_driver::TrisectCameraConfig>
      ReconfigureServer;
  boost::shared_ptr<ReconfigureServer> _reconfigureServer;

  void reconfigureCallback(TrisectCameraConfig &config, uint32_t level);
  TrisectCameraConfig _lastConfig;

  code_timing::CodeTiming code_timing_;

  // Diagnostic statistics
  std::shared_ptr<diagnostic_updater::HeaderlessTopicDiagnostic> _colorPubFreq;
  double _minFreq, _maxFreq;

  image_transport::ImageTransport image_transport_;

  // Periodic task (for publishing statistics)
  ros::Timer _periodicTimer;
  void periodicTask(const ros::TimerEvent &event);

  // Periodic task (for software trigger, if used)
  ros::Timer _software_trigger_timer;
};

}  // namespace trisect_camera_driver
