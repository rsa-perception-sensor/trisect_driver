
#include "trisect_camera_driver/mono_camera_handler.h"

#include "trisect_msgs/TrisectImagingMetadata.h"

namespace trisect_camera_driver {

MonoCameraHandler::MonoCameraHandler(
    ros::NodeHandle &nh, ros::NodeHandle &pnh, const std::string &name,
    const std::shared_ptr<libtrisect::V4LCamera> &camera,
    diagnostic_updater::Updater &updater, bool accelerated_image_proc)
    : name_(name),
      camera_(camera),
      info_manager_(std::make_shared<camera_info_manager::CameraInfoManager>(
          pnh, string("stereo_") + name)),
      image_transport_(nh),
      min_freq_(0.1),
      max_freq_(35),
      pub_freq_(make_shared<diagnostic_updater::HeaderlessTopicDiagnostic>(
          name + "_freq", updater,
          diagnostic_updater::FrequencyStatusParam(&min_freq_, &max_freq_, 0.1,
                                                   10))) {
  std::string calibration_topic(name_);
  calibration_topic += "_camera_info";

  float gaussian_sigma;
  pnh.param<float>("sterstereo_blur_sigmaeo_sigma", gaussian_sigma, 1.7);

  std::string calibrationUrl;
  pnh.param<std::string>(calibration_topic, calibrationUrl, "");
  ROS_INFO_STREAM("Left camera using calibration URL: " << calibrationUrl);
  if (calibrationUrl.size() > 0) {
    bool success = info_manager_->loadCameraInfo(calibrationUrl);
  }

  if (camera_) {
    {
      // RAW publisher
      std::string raw_topic(name_);
      raw_topic += "/image_raw";

      image_pub_ = std::make_shared<TrisectCameraPublisher>(
          name_, image_transport_.advertiseCamera(raw_topic, 1), info_manager_);
      camera_->addImageCallback(std::bind(&TrisectCameraPublisher::publish,
                                          image_pub_, std::placeholders::_1));
    }

    // Rectified publisher
    if (accelerated_image_proc) {
      std::string rect_topic("rectified/");
      rect_topic += name_;
      rect_topic += "/image_rect";

      undistort_pub_ = std::make_shared<UndistortPublisher>(
          name_, image_transport_.advertiseCamera(rect_topic, 1),
          info_manager_);
      camera_->addImageCallback(std::bind(&TrisectCameraPublisher::publish,
                                          undistort_pub_,
                                          std::placeholders::_1));
    }

    if (accelerated_image_proc) {
      std::string ds_topic("downsampled/");
      ds_topic += name_;
      ds_topic += "/image_rect";

      // Add downsampled publisher
      const int downsample = 2;
      downsampled_pub_ = std::make_shared<DownsamplePublisher>(
          pnh, name_, image_transport_.advertiseCamera(ds_topic, 1),
          info_manager_, downsample, gaussian_sigma);
      undistort_pub_->addOnPublishCallback(
          std::bind(&DownsamplePublisher::publish, downsampled_pub_,
                    std::placeholders::_1));
    }

    {
      // Add metadata publisher
      std::string meta_topic(name_);
      meta_topic += "/imaging_metadata";

      metadata_pub_ =
          nh.advertise<trisect_msgs::TrisectImagingMetadata>(meta_topic, 5);
    }

    image_pub_->addOnPublishCallback([this](const std_msgs::Header &header) {
      trisect_msgs::TrisectImagingMetadata meta;
      meta.header = header;

      meta.exposure_ns = camera_->exposure();
      meta.gain = camera_->gain();
      metadata_pub_.publish(meta);

      pub_freq_->tick();
    });
  }
}

}  // namespace trisect_camera_driver
