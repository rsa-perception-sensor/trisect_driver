
#include <cuda_runtime.h>
#include <cv_bridge/cv_bridge.h>
#include <image_geometry/pinhole_camera_model.h>
#include <image_transport/image_transport.h>
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <vpi/Image.h>
#include <vpi/LensDistortionModels.h>
#include <vpi/Stream.h>
#include <vpi/WarpMap.h>
#include <vpi/algo/Remap.h>

#include <mutex>
#include <vpi/OpenCVInterop.hpp>

#include "libtrisect/vpi_utils.h"

namespace trisect_camera_driver {

class VpiRectifyNodelet : public nodelet::Nodelet {
 public:
  VpiRectifyNodelet()
      : queue_size_(10),
        rectified_frame_suffix_(""),
        stream_(),
        initialized_(false),
        output_(nullptr) {}

  ~VpiRectifyNodelet() {
    vpiPayloadDestroy(warp_);
    vpiWarpMapFreeData(&map_);
  }

  void onInit() override {
    ros::NodeHandle &nh = getNodeHandle();
    ros::NodeHandle &private_nh = getPrivateNodeHandle();
    it_.reset(new image_transport::ImageTransport(nh));

    // Monitor whether anyone is subscribed to the output
    image_transport::SubscriberStatusCallback connect_cb =
        std::bind(&VpiRectifyNodelet::connectCb, this);

    // Make sure we don't enter connectCb() between advertising and assigning to
    // pub_rect_
    std::lock_guard<std::mutex> lock(connect_mutex_);

    pub_rect_ = it_->advertise("image_rect", 1, connect_cb, connect_cb);

    CHECK_STATUS(vpiStreamCreate(0, &stream_));
  }

  // Handles (un)subscribing when clients (un)subscribe
  void connectCb() {
    std::lock_guard<std::mutex> lock(connect_mutex_);
    if (pub_rect_.getNumSubscribers() == 0)
      sub_camera_.shutdown();
    else if (!sub_camera_) {
      image_transport::TransportHints hints("raw", ros::TransportHints(),
                                            getPrivateNodeHandle());
      sub_camera_ = it_->subscribeCamera(
          "image_raw", queue_size_, &VpiRectifyNodelet::imageCb, this, hints);
    }
  }

  void imageCb(const sensor_msgs::ImageConstPtr &image_msg,
               const sensor_msgs::CameraInfoConstPtr &info_msg) {
    std::lock_guard<std::mutex> lock(connect_mutex_);

    // Verify camera is actually calibrated
    if (info_msg->K[0] == 0.0) {
      NODELET_ERROR_THROTTLE(
          30,
          "Rectified topic '%s' requested but camera publishing '%s' "
          "is uncalibrated",
          pub_rect_.getTopic().c_str(), sub_camera_.getInfoTopic().c_str());
      return;
    }

    // If zero distortion, just pass the message along
    bool zero_distortion = true;
    for (size_t i = 0; i < info_msg->D.size(); ++i) {
      if (info_msg->D[i] != 0.0) {
        zero_distortion = false;
        break;
      }
    }
    // This will be true if D is empty/zero sized
    if (zero_distortion) {
      pub_rect_.publish(image_msg);
      return;
    }

    // Load image msg into vpi

    // Create cv::Mat views onto both buffers
    const cv::Mat image = cv_bridge::toCvShare(image_msg)->image;
    VPIImage input;
    CHECK_STATUS(
        vpiImageCreateWrapperOpenCVMat(image, VPI_BACKEND_CUDA, &input))

    int32_t input_width, input_height;
    vpiImageGetSize(input, &input_width, &input_height);

    VPIImageFormat input_type;
    vpiImageGetFormat(input, &input_type);

    if (!initialized_) {
      memset(&map_, 0, sizeof(map_));
      map_.grid.numHorizRegions = 1;
      map_.grid.numVertRegions = 1;
      map_.grid.regionWidth[0] = input_width;
      map_.grid.regionHeight[0] = input_height;
      map_.grid.horizInterval[0] = 1;
      map_.grid.vertInterval[0] = 1;
      CHECK_STATUS(vpiWarpMapAllocData(&map_));

      // \todo Check cinfo is "plumb bob"

      memset(&dist_model_, 0, sizeof(dist_model_));
      dist_model_.k1 = info_msg->D[0];
      dist_model_.k2 = info_msg->D[1];
      dist_model_.p1 = info_msg->D[2];
      dist_model_.p2 = info_msg->D[3];
      dist_model_.k3 = info_msg->D[4];
      dist_model_.k4 = dist_model_.k5 = dist_model_.k6 = 0.0;

      // n.b. These narrow from double to float which leads to warnings
      const VPICameraIntrinsic Kin = {
          {info_msg->K[0], info_msg->K[1], info_msg->K[2]},
          {info_msg->K[3], info_msg->K[4], info_msg->K[5]}};

      const VPICameraIntrinsic Kout = {
          {info_msg->P[0], info_msg->P[1], info_msg->P[2]},
          {info_msg->P[4], info_msg->P[5], info_msg->P[6]}};

      const VPICameraExtrinsic X = {
          {info_msg->R[0], info_msg->R[1], info_msg->R[2], 0},
          {info_msg->R[3], info_msg->R[4], info_msg->R[5], 0},
          {info_msg->R[6], info_msg->R[7], info_msg->R[8], 0},
      };

      CHECK_STATUS(vpiWarpMapGenerateFromPolynomialLensDistortionModel(
          Kin, X, Kout, &dist_model_, &map_));
      CHECK_STATUS(vpiCreateRemap(VPI_BACKEND_CUDA, &map_, &warp_));
      initialized_ = true;
    }

    if (output_ == nullptr) {
      CHECK_STATUS(
          vpiImageCreate(input_width, input_height, input_type, 0, &output_));
    } else {
      int32_t output_width, output_height;
      vpiImageGetSize(output_, &output_width, &output_height);

      VPIImageFormat output_type;
      vpiImageGetFormat(output_, &output_type);

      if ((output_type != input_type) || (output_height != input_height) ||
          (output_width != input_width)) {
        ROS_INFO("Format or size changed, rebuilding output VPIImage");
        vpiImageDestroy(output_);
        CHECK_STATUS(
            vpiImageCreate(input_width, input_height, input_type, 0, &output_));
      }
    }

    //
    // payload of "0" means "use the backend specified in the payload"
    CHECK_STATUS(vpiSubmitRemap(stream_, 0, warp_, input, output_,
                                VPI_INTERP_CATMULL_ROM, VPI_BORDER_ZERO, 0));
    CHECK_STATUS(vpiStreamSync(stream_));

    VPIImageData img_data;
    cv::Mat out_mat;
    CHECK_STATUS(vpiImageLockData(
        output_, VPI_LOCK_READ, VPI_IMAGE_BUFFER_HOST_PITCH_LINEAR, &img_data));
    CHECK_STATUS(vpiImageDataExportOpenCVMat(img_data, &out_mat));

    sensor_msgs::ImagePtr rect_msg =
        cv_bridge::CvImage(image_msg->header, image_msg->encoding, out_mat)
            .toImageMsg();
    // If 'rectified_frame_suffix_' is not empty, add to header
    if (!rectified_frame_suffix_.empty()) {
      rect_msg->header.frame_id += rectified_frame_suffix_;
    }
    pub_rect_.publish(rect_msg);

    CHECK_STATUS(vpiImageUnlock(output_));

    vpiImageDestroy(input);

    //   // Rectify and publish
    //   int interpolation;
    //   {
    //     std::lock_guard<std::recursive_mutex> lock(config_mutex_);
    //     interpolation = 0;
    //   }
    //   model_.rectifyImage(image, rect, interpolation);

    //   // Allocate new rectified image message
    //   sensor_msgs::ImagePtr rect_msg = cv_bridge::CvImage(image_msg->header,
    //   image_msg->encoding, rect).toImageMsg();
  }

 private:
  int queue_size_;
  std::string rectified_frame_suffix_;

  // Processing state (note: only safe because we're using single-threaded
  // NodeHandle!)
  image_geometry::PinholeCameraModel model_;

  std::mutex connect_mutex_;
  image_transport::Publisher pub_rect_;

  std::recursive_mutex config_mutex_;

  std::shared_ptr<image_transport::ImageTransport> it_;
  image_transport::CameraSubscriber sub_camera_;

  VPIStream stream_;
  VPIImage output_;

  VPIPolynomialLensDistortionModel dist_model_;
  bool initialized_;
  VPIWarpMap map_;
  VPIPayload warp_;
};

}  // namespace trisect_camera_driver

// Register nodelet
#include <pluginlib/class_list_macros.hpp>
PLUGINLIB_EXPORT_CLASS(trisect_camera_driver::VpiRectifyNodelet,
                       nodelet::Nodelet)
