#include "trisect_camera_driver/trisect_camera_publisher.h"

#include <cuda_runtime.h>
#include <cv_bridge/cv_bridge.h>
#include <vpi/Image.h>
#include <vpi/algo/GaussianFilter.h>
#include <vpi/algo/Remap.h>
#include <vpi/algo/Rescale.h>

#include <vpi/OpenCVInterop.hpp>

#include "libtrisect/trisect_cameras.h"
#include "libtrisect/vpi_utils.h"
#include "trisect_camera_driver/camera_info_conversions.h"
#include "trisect_msgs/TrisectImagingMetadata.h"

// For performance timing
#include <chrono>
using namespace std::chrono;

namespace trisect_camera_driver {

using cv_bridge::CvImage;

TrisectCameraPublisher::TrisectCameraPublisher(
    const std::string &name, image_transport::CameraPublisher pub,
    std::shared_ptr<camera_info_manager::CameraInfoManager> info_manager)
    : name_(name),
      image_publisher_(pub),
      info_manager_(info_manager),
      count_(0) {
  CHECK_STATUS(vpiStreamCreate(0, &stream_));
}

TrisectCameraPublisher::~TrisectCameraPublisher() {
  if (stream_) vpiStreamDestroy(stream_);
}

std_msgs::Header TrisectCameraPublisher::headerFromBuffer(
    const libtrisect::Camera::Buffer_t &buffer) {
  // Use time from the driver.
  std_msgs::Header header;

  // header.stamp.sec = buffer.driver_time.tv_sec;
  // header.stamp.nsec = buffer.driver_time.tv_nsec;

  header.stamp.sec = buffer.kernel_timestamp.tv_sec;
  header.stamp.nsec = buffer.kernel_timestamp.tv_usec * 1000;

  header.frame_id = name_;

  return header;
}

void TrisectCameraPublisher::publish(
    const libtrisect::Camera::Buffer_t &buffer) {
  count_++;

  VPIImageFormat format;
  CHECK_STATUS(vpiImageGetFormat(buffer.image, &format));

  std::string ros_encoding;
  int cv_type;

  if (format == VPI_IMAGE_FORMAT_U8) {
    // Alvium monochrome "stereo cameras"
    ros_encoding = "mono8";
  } else if (format == VPI_IMAGE_FORMAT_BGRA8) {
    ros_encoding = "bgra8";
  } else {
    ROS_ERROR_STREAM("Can't handle format " << std::hex << format);
    return;
  }

  // Unhandled formats
  //   if (buffer.pixformat == libtrisect::MonoCamera::FORMAT_GREY8) {
  //     // Alvium monochrome "stereo cameras"
  //     ros_encoding = "mono8";
  //     cv_type = CV_8UC1;
  //   } else if ((buffer.pixformat == libtrisect::MonoCamera::FORMAT_GREY10) ||
  //              (buffer.pixformat == libtrisect::MonoCamera::FORMAT_GREY12)) {
  //     ros_encoding = "mono16";
  //     cv_type = CV_16UC1;
  //   } else if (buffer.pixformat == V4L2_PIX_FMT_XBGR32) {
  //     // IMX477 color "operator camera"
  //     ros_encoding = "bgra8";
  //     cv_type = CV_8UC4;
  //   }

  VPIImageData img_data;
  cudaDeviceSynchronize();
  CHECK_STATUS(vpiImageLockData(buffer.image, VPI_LOCK_READ,
                                VPI_IMAGE_BUFFER_HOST_PITCH_LINEAR, &img_data));

  cv::Mat img;
  CHECK_STATUS(vpiImageDataExportOpenCVMat(img_data, &img));

  std_msgs::Header header(headerFromBuffer(buffer));
  cv_bridge::CvImage cv_img(header, ros_encoding, img);

  sensor_msgs::CameraInfoPtr cinfo_ptr(
      boost::make_shared<sensor_msgs::CameraInfo>(
          info_manager_->getCameraInfo()));
  cinfo_ptr->header = header;

  // n.b. toImageMsg() will _copy_ the data

  const auto img_msg = cv_img.toImageMsg();
  image_publisher_.publish(img_msg, cinfo_ptr);

  CHECK_STATUS(vpiImageUnlock(buffer.image));

  for (auto callback : on_publish_callbacks_) {
    callback(buffer);
  }
}

//===============

UndistortPublisher::UndistortPublisher(
    const std::string &name, image_transport::CameraPublisher pub,
    std::shared_ptr<camera_info_manager::CameraInfoManager> info_manager)
    : TrisectCameraPublisher(name, pub, info_manager),
      initialized_(false),
      output_(nullptr) {}

UndistortPublisher::~UndistortPublisher() {
  vpiPayloadDestroy(warp_);
  vpiWarpMapFreeData(&map_);

  if (output_) vpiImageDestroy(output_);
}

void UndistortPublisher::updateWarp(VPIImage input) {
  //
  //
  auto cinfo = info_manager_->getCameraInfo();

  int32_t input_width, input_height;
  vpiImageGetSize(input, &input_width, &input_height);

  memset(&map_, 0, sizeof(map_));
  map_.grid.numHorizRegions = 1;
  map_.grid.numVertRegions = 1;
  map_.grid.regionWidth[0] = input_width;
  map_.grid.regionHeight[0] = input_height;
  map_.grid.horizInterval[0] = 1;
  map_.grid.vertInterval[0] = 1;
  CHECK_STATUS(vpiWarpMapAllocData(&map_));

  // \todo Check cinfo is "plumb bob"

  memset(&dist_model_, 0, sizeof(dist_model_));
  dist_model_.k1 = cinfo.D[0];
  dist_model_.k2 = cinfo.D[1];
  dist_model_.p1 = cinfo.D[2];
  dist_model_.p2 = cinfo.D[3];
  dist_model_.k3 = cinfo.D[4];
  dist_model_.k4 = dist_model_.k5 = dist_model_.k6 = 0.0;

  // n.b. These narrow from double to float which leads to warnings
  const VPICameraIntrinsic Kin = {{cinfo.K[0], cinfo.K[1], cinfo.K[2]},
                                  {cinfo.K[3], cinfo.K[4], cinfo.K[5]}};

  const VPICameraIntrinsic Kout = {{cinfo.P[0], cinfo.P[1], cinfo.P[2]},
                                   {cinfo.P[4], cinfo.P[5], cinfo.P[6]}};

  const VPICameraExtrinsic X = {
      {cinfo.R[0], cinfo.R[1], cinfo.R[2], 0},
      {cinfo.R[3], cinfo.R[4], cinfo.R[5], 0},
      {cinfo.R[6], cinfo.R[7], cinfo.R[8], 0},
  };

  CHECK_STATUS(vpiWarpMapGenerateFromPolynomialLensDistortionModel(
      Kin, X, Kout, &dist_model_, &map_));
  CHECK_STATUS(vpiCreateRemap(VPI_BACKEND_CUDA, &map_, &warp_));
}

void UndistortPublisher::publish(const libtrisect::Camera::Buffer_t &buffer) {
  if (!initialized_) {
    updateWarp(buffer.image);
    initialized_ = true;
  }

  int32_t input_width, input_height;
  vpiImageGetSize(buffer.image, &input_width, &input_height);

  VPIImageFormat input_type;
  vpiImageGetFormat(buffer.image, &input_type);

  if (output_ == nullptr) {
    CHECK_STATUS(
        vpiImageCreate(input_width, input_height, input_type, 0, &output_));
  } else {
    int32_t output_width, output_height;
    vpiImageGetSize(output_, &output_width, &output_height);

    VPIImageFormat output_type;
    vpiImageGetFormat(output_, &output_type);

    if ((output_type != input_type) || (output_height != input_height) ||
        (output_width != input_width)) {
      ROS_INFO("Format or size changed, rebuilding output VPIImage");
      vpiImageDestroy(output_);
      CHECK_STATUS(
          vpiImageCreate(input_width, input_height, input_type, 0, &output_));
    }
  }

  //
  // payload of "0" means "use the one in the payload"
  CHECK_STATUS(vpiSubmitRemap(stream_, 0, warp_, buffer.image, output_,
                              VPI_INTERP_LINEAR, VPI_BORDER_ZERO, 0));
  CHECK_STATUS(vpiStreamSync(stream_));

  libtrisect::Camera::Buffer_t out_buffer(buffer);
  out_buffer.image = output_;

  TrisectCameraPublisher::publish(out_buffer);
}

//===============

DownsamplePublisher::DownsamplePublisher(
    ros::NodeHandle &pnh, const std::string &name,
    image_transport::CameraPublisher pub,
    std::shared_ptr<camera_info_manager::CameraInfoManager>
        upstream_info_manager,
    int downsample, float gaussian_sigma)
    : TrisectCameraPublisher(
          name, pub,
          std::make_shared<camera_info_manager::CameraInfoManager>(pnh, name)),
      downsample_(downsample),
      gaussian_sigma_(gaussian_sigma),
      output_(nullptr),
      blurred_(nullptr) {
  // Get camera info from upstream, reset and update my local info_manager_
  auto cinfo =
      scaleCameraInfo(upstream_info_manager->getCameraInfo(), downsample_);
  info_manager_->setCameraInfo(cinfo);
}

DownsamplePublisher::~DownsamplePublisher() {
  if (output_) vpiImageDestroy(output_);
}

void DownsamplePublisher::publish(const libtrisect::Camera::Buffer_t &buffer) {
  ensure_blurred_exists(buffer.image);
  ensure_output_exists(buffer.image);

  if (gaussian_sigma_ > 0) {
    CHECK_STATUS(vpiSubmitGaussianFilter(
        stream_, VPI_BACKEND_CUDA, buffer.image, blurred_, 5, 5,
        gaussian_sigma_, gaussian_sigma_, VPI_BORDER_ZERO));
    CHECK_STATUS(vpiSubmitRescale(stream_, VPI_BACKEND_CUDA, blurred_, output_,
                                  VPI_INTERP_CATMULL_ROM, VPI_BORDER_ZERO, 0));
  } else {
    CHECK_STATUS(vpiSubmitRescale(stream_, VPI_BACKEND_CUDA, buffer.image,
                                  output_, VPI_INTERP_CATMULL_ROM,
                                  VPI_BORDER_ZERO, 0));
  }
  CHECK_STATUS(vpiStreamSync(stream_));

  libtrisect::Camera::Buffer_t buffer_out(buffer);
  buffer_out.image = output_;

  TrisectCameraPublisher::publish(buffer_out);
}

// Opportunity to reduce DRY here...
//
VPIImage DownsamplePublisher::ensure_blurred_exists(VPIImage input) {
  int32_t input_width, input_height;
  vpiImageGetSize(input, &input_width, &input_height);

  VPIImageFormat input_type;
  vpiImageGetFormat(input, &input_type);

  if (blurred_ == nullptr) {
    CHECK_STATUS(
        vpiImageCreate(input_width, input_height, input_type, 0, &blurred_));
  } else {
    int32_t blurred_width, blurred_height;
    vpiImageGetSize(blurred_, &blurred_width, &blurred_height);

    VPIImageFormat blurred_type;
    vpiImageGetFormat(blurred_, &blurred_type);

    if ((blurred_type != input_type) || (blurred_height != input_height) ||
        (blurred_width != input_width)) {
      ROS_WARN("Format or size changed, rebuilding blurred VPIImage");
      vpiImageDestroy(blurred_);
      CHECK_STATUS(
          vpiImageCreate(input_width, input_height, input_type, 0, &blurred_));
    }
  }

  return blurred_;
}

VPIImage DownsamplePublisher::ensure_output_exists(VPIImage input) {
  int32_t input_width, input_height;
  vpiImageGetSize(input, &input_width, &input_height);

  VPIImageFormat input_type;
  vpiImageGetFormat(input, &input_type);

  if (output_ == nullptr) {
    ROS_WARN("Creating new output buffer");
    CHECK_STATUS(vpiImageCreate(input_width / downsample_,
                                input_height / downsample_, input_type, 0,
                                &output_));
  } else {
    int32_t output_width, output_height;
    vpiImageGetSize(output_, &output_width, &output_height);

    VPIImageFormat output_type;
    vpiImageGetFormat(output_, &output_type);

    if ((output_type != input_type) ||
        (output_height * downsample_ != input_height) ||
        (output_width * downsample_ != input_width)) {
      ROS_WARN_STREAM(
          "Format or size changed, rebuilding output VPIImage (downsample:"
          << downsample_ << ")");
      ROS_WARN_STREAM("Input: " << input_width << "x" << input_height << " --> "
                                << output_width << "x" << output_height);
      ROS_WARN_STREAM("Input: " << input_type << " --> " << output_type);

      vpiImageDestroy(output_);
      CHECK_STATUS(vpiImageCreate(input_width / downsample_,
                                  input_height / downsample_, input_type, 0,
                                  &output_));
    }
  }

  return output_;
}

}  // namespace trisect_camera_driver
