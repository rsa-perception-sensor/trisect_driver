#include <ros/ros.h>
#include <sensor_msgs/Image.h>

#include "trisect_camera_driver/trisect_driver.h"

using namespace trisect_camera_driver;

int main(int argc, char **argv) {
  ros::init(argc, argv, "trisect_driver_node");
  ros::NodeHandle nh;
  ros::NodeHandle pnh("~");

  auto d = TrisectDriver::CreateTrisectDriver(nh, pnh);

  if (!d) {
    ROS_FATAL_STREAM("Failed to create TrisectDriver: " << d.error().msg());
    return -1;
  }

  auto driver = d.value();

  driver->start();

  ros::AsyncSpinner spinner(2);
  spinner.start();

  ros::waitForShutdown();

  ROS_WARN("Shutting down TrisectNode.");

  driver->stop();

  return 0;
}
