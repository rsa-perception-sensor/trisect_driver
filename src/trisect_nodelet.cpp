#include <pluginlib/class_list_macros.h>
#include <sensor_msgs/Image.h>

#include "trisect_camera_driver/trisect_driver.h"

namespace trisect_camera_driver {

class TrisectNodelet : public nodelet::Nodelet {
 public:
  TrisectNodelet();
  virtual ~TrisectNodelet();

  virtual void onInit();

 private:
  TrisectDriver::shared_ptr _driver;
};

TrisectNodelet::TrisectNodelet() : Nodelet(), _driver(nullptr) { ; }

TrisectNodelet::~TrisectNodelet() {
  if (_driver) _driver->stop();
}

void TrisectNodelet::onInit() {
  ros::NodeHandle &nh = getNodeHandle();
  ros::NodeHandle &pnh = getPrivateNodeHandle();

  auto d = TrisectDriver::CreateTrisectDriver(nh, pnh);

  if (!d) {
    NODELET_FATAL_STREAM(
        "Error when initializing TrisectDriver: " << d.error().msg());
    return;
  }

  _driver = d.value();
  _driver->start();
}

}  // namespace trisect_camera_driver

PLUGINLIB_EXPORT_CLASS(trisect_camera_driver::TrisectNodelet, nodelet::Nodelet)
