
#include "trisect_camera_driver/color_camera_handler.h"

#include "trisect_msgs/TrisectImagingMetadata.h"

namespace trisect_camera_driver {

ColorCameraHandler::ColorCameraHandler(
    ros::NodeHandle &nh, ros::NodeHandle &pnh, const std::string &name,
    const std::shared_ptr<libtrisect::GstCamera> &camera,
    diagnostic_updater::Updater &updater, bool accelerated_image_proc)
    : name_(name),
      camera_(camera),
      info_manager_(
          std::make_shared<camera_info_manager::CameraInfoManager>(pnh, name)),
      image_transport_(nh),
      min_freq_(0.1),
      max_freq_(35),
      pub_freq_(make_shared<diagnostic_updater::HeaderlessTopicDiagnostic>(
          name + "_freq", updater,
          diagnostic_updater::FrequencyStatusParam(&min_freq_, &max_freq_, 0.1,
                                                   10))) {
  std::string calibration_topic(name_);
  calibration_topic += "_camera_info";

  std::string calibrationUrl;
  pnh.param<std::string>(calibration_topic, calibrationUrl, "");
  ROS_INFO_STREAM(name_ << " camera using calibration URL: " << calibrationUrl);
  if (calibrationUrl.size() > 0) {
    bool success = info_manager_->loadCameraInfo(calibrationUrl);
  }

  if (camera_) {
    {
      // RAW publisher
      std::string raw_topic(name_);
      raw_topic += "/image_raw";

      image_pub_ = std::make_shared<TrisectCameraPublisher>(
          name_, image_transport_.advertiseCamera(raw_topic, 1), info_manager_);
      camera_->addImageCallback(std::bind(&TrisectCameraPublisher::publish,
                                          image_pub_, std::placeholders::_1));
    }

    // Do not enable this;  current version leaks memory and crashes Trisect
    if (false) {
      std::string ds_topic("downsampled/");
      ds_topic += name_;
      ds_topic += "/image_raw";

      // Add downsampled publisher

      // \todo{}  This should be configurable
      const int downsample = 2;
      const float color_sigma = 1.7;
      downsampled_pub_ = std::make_shared<DownsamplePublisher>(
          pnh, name_, image_transport_.advertiseCamera(ds_topic, 1),
          info_manager_, downsample, color_sigma);
      camera_->addImageCallback(std::bind(&DownsamplePublisher::publish,
                                          downsampled_pub_,
                                          std::placeholders::_1));
    }
  }
}

}  // namespace trisect_camera_driver
